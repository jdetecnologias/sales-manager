import React,{useState} from 'react'
import {FormGroup, Label, Input, Col, Button} from 'reactstrap'

import Formulario from '../utils/formulario'

import * as U from '../utils/utils'

import axios from 'axios'

import {gravarCliente, VerificarSeClienteExiste, RetornarClienteSessao, atualizarCliente} from '../controller/cliente.controller'
import {getCEP} from '../controller/util.controller'

const CadCliente = (props)=>{

	const [cliente, setCliente] = useState(obterNovoCLiente())
	
	const modo = props.exibirCliente?'editar':'gravar'
	let stringcliente = null
	if (modo === 'editar'){
		stringcliente = U.ObjectToString(RetornarClienteSessao())
	}

	const propriedadesCliente = Object.keys(cliente)
	const [camposVazios, setCamposVazios] = useState([])
	
	function FiltrarDadosCliente(){
		const propriedades = Object.keys(cliente)
		
		let obj = {}
		
		propriedades.forEach(prop=>{
			obj[prop] = cliente[prop].value
		})
		
		return obj
	}
	
	function validarCliente(){ 
		function clienteExiste(){
			const clienteExiste = VerificarSeClienteExiste(cliente.identificador.value)
			
			if (clienteExiste){
				alert('cliente já está cadastrado')
			}
			
			return clienteExiste
		}
		
		function estarPreenchidoCorretamente(){
				let Resultado = true
				let lCamposVazios = []
				
				propriedadesCliente.forEach(prop=>{
					if(cliente[prop].value === ""){
						lCamposVazios.push(prop)
						Resultado =  false;
					}
				})
				
				if(!Resultado){
					alert('Os seguintes campos estão vazios: '+lCamposVazios.join(', ')+'. Favor preenche-los e tentar novamente')
					setCamposVazios(camposVazios=>lCamposVazios)
				}else{
					setCamposVazios(camposVazios=>[])
				}
				return Resultado
		}
		
		if(modo === 'gravar'){			
			return   estarPreenchidoCorretamente() && !clienteExiste()
		}else if(modo === 'editar'){
			return estarPreenchidoCorretamente()
		}	
		
	}
	
	function cadastrarCliente(AdadosCliente){
		if(validarCliente()){
			
			if(modo === 'gravar'){
				gravarCliente(AdadosCliente)
			}else{
				atualizarCliente(AdadosCliente)
			}
			
			setCliente(cliente=> obterNovoCLiente())			
		}  	
	}
	
	
	function ContinuarProcessamento(){
		if(modo === 'gravar'){
			gravarCliente(FiltrarDadosCliente())
		}else if(modo === 'editar'){
			if(stringcliente !== U.ObjectToString(FiltrarDadosCliente())){
			 props.eventoFinalizador(FiltrarDadosCliente())  	
			}
		}


	}

	function obterNovoCLiente(){
		let objetoCliente = {
				name:{value:"",largura:'12', tipo:'text', label:'Nome'},
				email:{value:"",largura:'12', tipo:'text', label:'E-mail'},
				registerNumber:{value:"",largura:'12', tipo:'text', label:'CPF/CNPJ'},
				zipCode:{value:"",largura:'5', tipo:'text', label:'CEP'},
				publicPlace:{value:"",largura:'7', tipo:'text', label:'Logradouro'}, 
				number:{value:"",largura:'4', tipo:'text', label:'Núm.'},
				complement:{value:"",largura:'8', tipo:'text', label:'Complemento'},
				district:{value:"",largura:'6', tipo:'text', label:'Bairro'},
				city:{value:"",largura:'6', tipo:'text', label:'Cidade'},
				state:{value:'',options:['GO', 'MG'],largura:'4', tipo:'select', label:'Estado'},
				phoneNumber:{value:"",larglargura:'8', tipo:'text', label:'Telefone'}
				}
				
		if(props.exibirCliente){
			const clienteSessao = RetornarClienteSessao()
			
			const propsCliente = Object.keys(clienteSessao)
			
			propsCliente.forEach(pro=>{
				objetoCliente[pro].value = clienteSessao[pro]
			})
		}
		return objetoCliente
	}
	
	function modificarState(AEvent){
		
		const lnomeprop = AEvent.target.getAttribute('name')
		const lvalorProp = AEvent.target.value
		
		let newCliente = {...cliente}
		if(lnomeprop === 'cep'){
				const resultado = getCEP(lvalorProp)
				
				if (resultado){
					newCliente.logradouro.value = resultado.logradouro
					newCliente.uf.value = resultado.uf
					newCliente.cidade.value = resultado.localidade
					newCliente.bairro.value = resultado.bairro	
				}
		}
		
		newCliente[lnomeprop].value = lvalorProp
		
		setCliente(cliente=>newCliente)
	}
	
	function gerarCamposFormulario(prop){
		return <Formulario 
					className={camposVazios.some(cp=>cp===prop)?'campoIncorreto':''}
					type={cliente[prop].tipo} 
					col={cliente[prop].largura}
					label={cliente[prop].label}
					placeholder={cliente[prop].label} 
					elementEvent={(e)=>modificarState(e)}
					value={cliente[prop].value}
					name={prop}
					options={cliente[prop].options || []}
					/>
	}
	
	return (
		<div className='col-12'>
			<h3>
				Cadastrar clientes
			</h3>
			<div className='row'>
			
				
			{
				propriedadesCliente.map(propriedade=>gerarCamposFormulario(propriedade))
			}
			
			<Button size='lg' className='btn-block' color='primary'onClick={ContinuarProcessamento}>Continuar</Button>
			<Button size='lg' className='btn-block' color='danger' onClick={()=> setCliente(obterNovoCLiente())}>Limpar</Button>
			</div>
			
		</div>
	)
}

export default CadCliente