import React,{useState} from 'react'
import FormView from './component.form.view'

import {AdicionarProduto} from '../controller/produto.controller'
import {criarEstoque} from '../controller/estoque.controller'

function Produto(props){
	const objProduto = {
		name:{value:"",largura:'12', type:'text', label:'Nome'}, 
		price:{value:"",largura:'6', type:'text', label:'Preço'}, 
		weight:{value:"",largura:'6', type:'text', label:'Peso'}, 
		sku:{value:"",largura:'12', type:'text', label:'Código produto'},
		tamanhos:{nodata:true,value:"", options:['P','M','G','GG'],type:'checkbox', page:'own',onchange:adicionarVarianteNoProduto},
		cores:{nodata:true, value:"", options:['Azul bic', 'Verde', 'Preto', 'Vermelho', 'Amarelo', 'Branco','Pink'], type:'checkbox', page:'own',onchange:adicionarVarianteNoProduto}
	}

	const [modal, setModal] = useState(false)
	const [produto, setProduto] = useState(objProduto)
	function alternar(){
		setModal(!modal)
	}
	
	function adicionarVarianteNoProduto(aVariante, aItemVariante){
		
		let variantes = produto.variantes
		const isEmptyVarianteProduto = Object.keys(variantes).length == 0
		
		if(isEmptyVarianteProduto || variantes[aVariante] == undefined){
			variantes[aVariante] = new Array(aItemVariante)
			
		}else{
			const indiceItemEncontrado =  variantes[aVariante].indexOf(aItemVariante)
		
			
			if(indiceItemEncontrado == -1){
				variantes[aVariante].push(aItemVariante)
			}else{
				variantes[aVariante].splice(indiceItemEncontrado,1)
			}
		}
		const NewProduto = {...produto}
		NewProduto.variantes = variantes
		
		setProduto(produto=>NewProduto)
		
	}
	
	function verificarSeVarianteEstaNoProduto(aVariante,aItemVariante){
		
		const variantes = produto.variantes
		const isEmptyVarianteProduto = Object.keys(variantes).length == 0
		
		if(isEmptyVarianteProduto){
			return false
			
		}else{
			const indiceItemEncontrado = variantes[aVariante] != undefined ? variantes[aVariante].indexOf(aItemVariante): -1
			
			if(indiceItemEncontrado == -1){
				return false
			}else{
				return true
			}
		}
	}
	
	
	function gravarProduto(AProduto){
		
		AdicionarProduto(AProduto)
		 const produtoVazio = {...objProduto}
		setProduto(AProduto=>produtoVazio)
	}

	return (
		<
		FormView formData={objProduto} 
		registerButtonLabel='Gravar' 
		cancelButtonLabel='Cancelar'
		onClickRegister={gravarProduto}
		/>
	)
	
}

export default Produto