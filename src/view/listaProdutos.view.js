import React, {useState}  from 'react'
import * as U from '../utils/utils'
import MostrarPedido from './mostrarPedido.view'
import {getIdVariante} from '../funcoes/funcoes'
import {getQtdItem, verificarSeExisteEstoque, alocarEstoque} from '../controller/estoque.controller'
import {gravarPedido, obterPedido, excluirItemPedido} from '../controller/pedido.controller'
import {Button, Collapse, Card, CardBody} from 'reactstrap'
import InputQtd from '../utils/inputQty'

const ListaProdutos = (props)=>{
	
const produtos = U.getLSItem('produtos')	
const [pedido, setPedido] = useState(obterPedido())
const [item, setItem] = useState(obterItemVazio())
const lEstoque = ()=> U.getLSItem('estoque')

const [estoque, setEstoque] = useState(lEstoque());

function obterItemVazio(){
	return {id:null, idProduto:null, tamanho:null, cor:null, qtd:1, preco: null, descricao:null, qtdDisponivelEstoque:0}
}

function definirVariante(nomeVariante, valor,idProdutoPrincipal, preco, descricao, evento){
	const newItem = {...item}

	newItem.id = idProdutoPrincipal
	
	newItem.preco = preco
	
	newItem.descricao = descricao 

	newItem[nomeVariante] = valor
	
	newItem.idProduto = getIdVariante(idProdutoPrincipal, newItem.tamanho, newItem.cor)
	
	setEstoque(estoque=>[...estoque])
	
	const TemProdutoNoEstoque = verificarSeExisteEstoque(newItem.idProduto, estoque , newItem.qtd) 
	
	if(!TemProdutoNoEstoque){
		evento.target.checked = false
	}
	
	setItem(item=>newItem)
}

function alternarSelecaoRadio(e, nomeVariante){
	
	const EstaSelecionado = e.target.checked
	if (EstaSelecionado == true && verificarVarianteSelecionado(nomeVariante)){
		e.target.checked = false
		
		const NewItem = {...item}
	
		NewItem[nomeVariante] = null
		
		setItem(item=>NewItem)
	}
	

	
	
}

function verificarSeVarianteEstaSelecionado(){
	return verificarVarianteSelecionado("tamanho") && verificarVarianteSelecionado("tamanho") != null
}

function verificarVarianteSelecionado(nomeVariante){
	return item[nomeVariante] != null
}

function estoqueExiste(lId, estoque , Aqtd){
	const TemProdutoNoEstoque = verificarSeExisteEstoque(lId, estoque , Aqtd) 
	
	if( !TemProdutoNoEstoque ){
		
		alert('Saldo em estoque insuficiente!')
		
		deselecionarRadioButtons()
		setItem(item=>obterItemVazio())
	}	
	
	return TemProdutoNoEstoque
	
}

function definirQtd(Aid, Aqtd){
	const NewItem = {...item}
	
	const lId = item.idProduto == null ? Aid : item.idProduto
	
	const TemProdutoNoEstoque = estoqueExiste(lId, estoque , Aqtd) 
		
	if( !TemProdutoNoEstoque && Aqtd >= item.qtd && item.idProduto || !verificarSeVarianteEstaSelecionado()){
		
		NewItem.qtd = item.qtd
		
		if(!verificarSeVarianteEstaSelecionado()){
			alert('Favor selecionar o Produto. Tamanho e Cor')
		}
		
	}else{
		NewItem.qtd = Aqtd
	}
	
	NewItem.id = Aid
	
	setItem(item=>NewItem)
}

function excluirItem(id){
	
	excluirItemPedido(id)

	setPedido(pedido=>obterPedido())
	
	setEstoque(estoque=>lEstoque())
}


function verificarEstoque(nomeVariante, valorVariante, idProdPai,Aestoque, qtd){
	
	let resultado = false
	if(nomeVariante === "tamanho"){
		const idVariante = getIdVariante(idProdPai, valorVariante, null)

		resultado =  verificarSeExisteEstoque(idVariante, Aestoque, qtd)
	}else if(nomeVariante === "cor" && item.idProduto != null){
		resultado =  verificarSeExisteEstoque(item.idProduto, Aestoque, qtd)
	}
	
	return resultado 
	
}

function getQtdItem(AidProdutoPai){
	
	if(AidProdutoPai == item.id){
		return item.qtd
	}else{
		return 1
	}
	
}

function apagarPedidoView(){	
	setPedido(pedido=>obterPedido())
}	
	

function adicionarItemAoPedido(){
	
	const TemNoEstoque = estoqueExiste(item.idProduto, estoque , item.qtd)
	
	const  somePropertyisNull = Object.keys(item).some(it=>item[it]==null)
	
	if(TemNoEstoque){	
		if(!somePropertyisNull){
		const newPedido = {...pedido}
		
		newPedido.itens.push(item)
		
		alocarEstoque(item.idProduto, item.qtd)
				
		setPedido(pedido=>newPedido)
		
		setItem(item=>obterItemVazio())
		
		setEstoque(estoque=>lEstoque())
		}
		
		 deselecionarRadioButtons()
	}
}

function deselecionarRadioButtons(){
	const checkbox = document.querySelectorAll('input[type=radio]:checked')
	
	checkbox.forEach(check=>{
		check.checked = false
	})
	
}

	return (
		<div className='container'>
			<MostrarPedido pedido={pedido} exclusao={excluirItem} apagarPedido={apagarPedidoView}/>
			<h3 className='text-center'>Lista de produtos</h3>
		{
			produtos.map(produto=>{
				return (<div className='bg-light text-dark mt-2'>
							<ul>
								<li className='text-center'>{produto.nome}</li>							
								<li className='row'>
									<div className='col-6'>
										<img src='https://lh3.googleusercontent.com/pw/ACtC-3ePlfn5gKJUBV07x4vYiW3NxAZzs3U3e69HToqmdAfRymIRne_zkoG_R3mJldmqg9hJB8UAK2KB4bzQt_9KJlWC4cc-q39rux4zFY59OtBgGmkYUoyS1NTxJPAXfxstZwoTgSttyaScGHvHPlow6ia04A=w130-h162-no?authuser=0' className='w-100'/>
									</div>
									<div className='col-6 p-0 m-0'>
										<ul className='p-0 m-0' id="info-produto">
											<li> R$ {produto.preco} </li>
											<li>
												<InputQtd qty={getQtdItem(produto.id)} onClick={(qtd)=>definirQtd(produto.id, qtd)}/> 
											</li>	
											<li><button onClick={adicionarItemAoPedido} className='btn btn-success w-50'>Add</button></li>
																				
										</ul>	
									</div>
								</li>
								<li>
									{
									Object.keys(produto.variantes).map(variante=>{
										return(<div className='mt-1 ml-1'>
										
											{variante}
											<div className='col-12 m-0 p-0'>
												{produto.variantes[variante].map((varr)=>{
													const existeEstoque = !verificarEstoque(variante, varr,produto.id, estoque, item.qtd )
													return(<span>			
																<input 
																	type='radio' 
																	onChange={(e)=>definirVariante(variante, varr, produto.id, produto.preco, produto.nome,e)} 
																	name={produto.id+variante} 																	
																	className='mt-1 ml-1' 
																	value={varr}
																	disabled={existeEstoque}
																	onClick={(e)=>alternarSelecaoRadio(e, variante)}
																/>{varr}
														   </span>)
													}
												)} 
											</div>
										</div>)})
									}
								</li>							
							</ul>							
						</div>)
			})
		}
		</div>
		)
}

export default ListaProdutos