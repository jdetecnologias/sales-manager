import React, {useState} from 'react'
import { Table, Input } from 'reactstrap';

import * as U from '../utils/utils'

import {RetornarDadosTabela, getIdVariante} from '../funcoes/funcoes'

import {setProdutosMook, CEP} from '../utils/mookUp'

import {setQtdItem, getQtdItem} from '../controller/estoque.controller'

setProdutosMook()
CEP()

let produtos = U.getLSItem('produtos')


produtos = produtos == null ? [] : produtos

const Estoque = (props)=>{
	
const [estoque,setEstoque] = useState(U.getLSItem('estoque'))	

function setQtdItemEstoque(id, qtd){
	
	setQtdItem(id,qtd)
	
	setEstoque(U.getLSItem('estoque'))
	
}

	return (
			<div>			
				<h5>Estoque</h5>
				<Table>
					<tr><td>Nome</td><td>Qtd. disp</td><td>qtd. Aloc.</td><td>Tota.</td></tr>
					{
						 produtos.map(produto=>{	 
							return (<tbody>
							{
							 RetornarDadosTabela(produto).map((lv,key)=>{
								 const qtdDisp = getQtdItem(lv.id, estoque)
								 const qtdAloc = getQtdItem(lv.id, estoque, "alocado")
								 if(key == 0){
									return (<tr>
												<td className='w-75'><b><i>{lv.nome}</i></b></td>
												<td className='w-25'>
													<Input onChange={(e)=>setQtdItemEstoque(lv.id, e.target.value)} value={qtdDisp} type='text'/>
												</td>
												<td>{qtdAloc}</td>
												<td>{parseFloat(qtdDisp)+parseFloat(qtdAloc)}</td>
											</tr>) 									 
								 }else{
									return (<tr>
												<td>{lv.nome}</td>
												<td>
													<Input onChange={(e)=>setQtdItemEstoque(lv.id, e.target.value)} value={qtdDisp}type='text'/>
												</td>
												<td>{qtdAloc}</td>
												<td>{parseFloat(qtdDisp)+parseFloat(qtdAloc)}</td>
											</tr>)
								 }
							})
							}
							</tbody>)
							 
						})

					}
				</Table>
			</div>
			
			)
}

export default Estoque