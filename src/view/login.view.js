import react,{useState} from 'react'

import Formulario from '../utils/formulario'

import {Button} from 'reactstrap'

const LoginPage = props =>{
	const [email, setEmail] = useState('')
	const [pass, setPass] = useState('')
	
	return (
				<div className="container-fluid col-12">
					<div className="mt-5">
						<h3 className="text-center">Login</h3>
						<Formulario type="text" label="E-mail" placeholder="E-mail" name="senha" value={email} elementEvent={(e)=>setEmail(e.target.value)}/>
						
						<Formulario type="password" label="Senha" placeholder="Senha" name="senha" value={pass} elementEvent={(e)=>setPass(e.target.value)}>
							<a href='/recuperar/senha'>Esqueci a senha</a> | <a href="/cadastro">Cadastrar-se</a>
						</Formulario>
						
						<Button color="primary">Entrar</Button>
						<Button color="danger">Cancelar</Button>
					</div>
				
				</div>
			
	)
}

export default LoginPage