import React,{useState} from 'react'
import {Collapse, Button, CardBody, Card, Table, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap'
import {gravarPedido, obterPedido,apagarPedido, finalizarPedido} from '../controller/pedido.controller'
import If from '../utils/if'
import CadCliente from './cadastroCliente.view'


const MostrarPedido = (props)=>{
	const [isOpen, setIsOpen] = useState(false)
	const toggle = ()=>{
		const lIsOpen = isOpen
		
		setIsOpen(!isOpen)
		
		if(lIsOpen) setExibeCliente(false)
		}
	const [exibeCliente, setExibeCliente] = useState(false)
	gravarPedido(props.pedido)
	
	function apagarPedidoView(){
		const querApagarMesmo = window.confirm('O pedido será apagado. Deseja continuar?')
		
		if(querApagarMesmo){
			apagarPedido()
			props.apagarPedido()
		}
	}	
	
	function ObterDadosCLiente(){
		setExibeCliente(true)
	}
	
	function enviarPedido(cliente){
		
		props.pedido.dadosCliente = cliente

		finalizarPedido(props.pedido)
		
		alert('pedido realizado com sucesso!')
		
		props.apagarPedido()
		
		setExibeCliente(false)
	}
	


	return (<div>
				<Button style={{position:'fixed', right:'0px', zIndex:'999'}} color='primary' onClick={toggle}>P</Button>
				<Modal isOpen={isOpen} toggle={toggle}>
					<ModalHeader toggle={toggle}>Pedido</ModalHeader>
						<ModalBody className='p-0 m-0'>
							<If test={!exibeCliente} Else={<CadCliente eventoFinalizador={enviarPedido} exibirCliente={true}/>}>
								<div>
									{props.pedido.itens.map(item=>{
									return	(<div className='row text-center'>
												<div className='col-12 row'>
													<div className='col-9'>{item.descricao+" "+item.cor+" "+item.tamanho}</div>												
													<div className='col-3'><Button size='sm' color='danger' onClick={()=>props.exclusao(item.idProduto)}>X</Button></div>
												</div>
												<div  className='col-12 row'>
													<div  className='col-4'>Qtd</div>
													<div  className='col-4'>V.Unit</div>
													<div  className='col-4'>V. Total</div>
												</div>
												<div  className='col-12 row'>
													<div  className='col-4'>{item.qtd}</div>
													<div  className='col-4'>{item.preco}</div>
													<div  className='col-4'>{(item.qtd*item.preco)}</div>
												</div>
											</div>)	
									})}
									<If test={props.pedido.itens.length > 0}>
										<Button color='success ml-1' onClick={ObterDadosCLiente}>Enviar Pedido</Button>
										<Button color='danger' className='ml-1' onClick={()=>apagarPedidoView()}>Cancelar pedido</Button>
									</If>
								
								</div>
							</If>
						</ModalBody>
						<ModalFooter>
							<Button color='danger' onClick={toggle}>Fechar</Button>
						</ModalFooter>
					
				</Modal>
			</div>)
}

export default MostrarPedido