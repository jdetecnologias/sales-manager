import React, {useState} from 'react';
import Formulario from '../utils/formulario'
import ModalForm from '../utils/modal'


const FormView = (props)=>{

const [FformData, setFormData] = useState(props.formData)

const proprietiesFormData = Object.keys(FformData)

const emptyFields = []

 function parseData(){
     let lObj = {}
     proprietiesFormData.forEach(prop=>{
         
            if (FformData[prop].nodata === undefined){
                lObj[prop] = FformData[prop].value
            }   	
     })

     return lObj
 }

    function generatesForm(){
       return  proprietiesFormData.map(prop=>{
           
            if(FformData[prop].page === undefined || FformData[prop].page === 'own' ){
                return <Formulario 
                className={emptyFields.some(cp=>cp===prop)?'campoIncorreto':''}
                type={FformData[prop].type} 
                col={FformData[prop].largura}
                label={FformData[prop].label}
                placeholder={FformData[prop].label} 
                elementEvent={(e)=>changeState(e)}
                value={FformData[prop].value}
                name={prop}
                options={FformData[prop].options || []}
                />         
            }else if(FformData[prop].page === 'modal'){
                return (<ModalForm label='teste' name='teste'>
                   <Formulario 
                className={emptyFields.some(cp=>cp===prop)?'campoIncorreto':''}
                type={FformData[prop].type} 
                col={FformData[prop].largura}
                label={FformData[prop].label}
                placeholder={FformData[prop].label} 
                elementEvent={(e)=>changeState(e)}
                value={FformData[prop].value}
                name={prop}
                options={FformData[prop].options || []}
                />
                </ModalForm>)
            }


        })

    }

	function changeState(AEvent){
        
		if(props.beginnigUpdate) props.beginnigUpdate()        
		const lnameProp = AEvent.target.getAttribute('name')
		const lvalueProp = AEvent.target.value
		
		let newFormData = {...FformData}

		if(props.beforeUpdate) props.beforeUpdate(newFormData)

		newFormData[lnameProp].value = lvalueProp
		
        setFormData(formData=>newFormData)

		if(props.afterUpdate) props.afterUpdate()        
	}    

    return (
        <div className='container'>
            <div className='row'>
                {generatesForm()}
                <button className='btn-block btn-lg btn-primary' color='primary' onClick={()=>props.onClickRegister(parseData())}>{props.registerButtonLabel}</button>
			    <button className='btn-block btn-lg btn-danger' color='danger' onClick={props.onClickCancell} >{props.cancelButtonLabel}</button>                
            </div>
            <div>
                <h5>Lista produtos</h5>
            </div>
        </div>
    )
}

export default FormView