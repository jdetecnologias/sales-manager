import React from 'react';

const InputQtd  = props=>{
	
   function ModificarQtd(limite, valorOperacao, valorAtual){
		
        if(parseInt(valorAtual) !== parseInt(limite)){
			const valorAtualizado = valorAtual+valorOperacao
			props.onClick(valorAtualizado)
        }
    }

    return (    
              <div>
                  <button className='btn btn-primary btn-sm' onClick={()=>ModificarQtd(1,-1,props.qty)}>-</button>
                  {props.qty}
                  <button className='btn btn-primary btn-sm' onClick={()=>ModificarQtd(props.maximo || 99,+1,props.qty)}>+</button>
              </div>   
          )

}


export default InputQtd;