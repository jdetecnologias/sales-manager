export function getLSItem(item){
	return JSON.parse(localStorage.getItem(item))
}

export function setLSItem(item, valor,){
	localStorage.setItem(item, JSON.stringify(valor))
}


export function IsNotUniqueInArray(arr, campo, id){
	return arr.some(a=>a[campo] == id)
}

export function getSSItem(item){
	return JSON.parse(sessionStorage.getItem(item))
}

export function setSSItem(item, valor,){
	sessionStorage.setItem(item, JSON.stringify(valor))
}

export function generateRandomId(){
	return new Date().getTime()
}

export function ObjectToString(Aobj){
	let lString = ''
	
	Object.keys(Aobj).forEach(prop=> lString+= Aobj[prop])
	
	return (lString.replace(' ',''))
}