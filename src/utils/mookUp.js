import * as U from '../utils/utils'

import {criarEstoque} from '../controller/estoque.controller'

export function setProdutosMook(){
	let produtos = U.getLSItem('produtos')
	if (produtos == null){
		produtos = [
						{
						id:U.generateRandomId(), nome:'Tshirt A' , preco:'13', peso:'0.13', variantes:{tamanho:['P','M','G','GG'], cor:['branco','vermelho','azul bic','verde','preto']}
						},
						{
						id:U.generateRandomId()+1,	nome:'Tshirt B' , preco:'13', peso:'0.13', variantes:{tamanho:['P','G','GG'], cor:['branco','verde','preto']}
						},
						{
						id:U.generateRandomId()+2,	nome:'Tshirt C', preco:'13', peso:'0.13', variantes:{tamanho:['P','M','G','GG','Croped'], cor:['azul bic','preto']}
						}		
					]
					
		U.setLSItem('produtos', produtos)
	}
	
	
	
	produtos.forEach(produto=>{
		criarEstoque(produto)
	})
	

}

export function CEP(){
	U.setLSItem('cep', {
  "cep": "75254-647",
  "logradouro": "Rua Ipameri",
  "complemento": "",
  "bairro": "Vila Galvão",
  "localidade": "Senador Canedo",
  "uf": "GO",
  "ibge": "5220454",
  "gia": "",
  "ddd": "62",
  "siafi": "9753"
})
}