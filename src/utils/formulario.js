import React from 'react'


const Formulario = props=>{
	
	function getClassName(){
		let className = 'form-group'	
		
		const larguraElemento = props.col?'col-'+props.col:''
		
		className += ' '+larguraElemento
		
		if(props.className !== undefined && props.className !== ""){
					
			if(props.className.indexOf('col-') > -1) {
				className = className.replace(larguraElemento, '')
			}
			
			const todasAsClassName = props.className.split(' ')
			
			todasAsClassName.forEach(classNm=>{
				className +=" "+ className.indexOf(classNm) > -1 ? '': ' '+classNm
			})
		}
		return className
	}
	
	function retornarElementoFormulario(tipo){
		
		switch(tipo){
			case 'text':
				return <InputText 
							onChange={props.elementEvent === undefined?'':props.elementEvent} 
							placeholder={props.placeholder === undefined?'':props.placeholder}
							value={props.value}
							name={props.name}
						/>
			break;
			case 'password':
				return <InputPassword
							onChange={props.elementEvent === undefined?'':props.elementEvent} 
							placeholder={props.placeholder === undefined?'':props.placeholder}
							value={props.value}
							name={props.name}
						/>
			break;			
			case 'radio':
				return <InputRadio onChange={props.elementEvent === undefined?'':props.elementEvent}  />
			break;
			case 'checkbox':
				return props.options.map(option=>{
					return <InputCheckBox 
						onChange={props.elementEvent === undefined?null:props.elementEvent} 
						value={option}
						/>
						
				})
				
			break;						
			case 'select':
				return <Select 
						onChange={props.elementEvent === undefined?'':props.elementEvent} 
						options={props.options}
						value={props.value}
						name={props.name}
						/>
			break;	
		}
	}
	
return (
				 <div className={getClassName()}>
					<label for={props.label}>{props.label}</label>
					{
						retornarElementoFormulario(props.type)
					
					}
					  <small class="form-text text-muted">{props.children !== undefined? props.children:  false}</small>
				</div>
					
		)

	
}

const InputText = (props)=>{
	
	return <input type='text' {...props} className='form-control'/>
}

const InputPassword = (props)=>{
	
	return <input type='password' {...props} className='form-control'/>
}

const InputRadio = (props)=>{	
	return <input type='radio' {...props}/>
}

const InputCheckBox = (props)=>{	
	return <><input type='checkbox' {...props}/>{props.value}</>
}

const Select = (props)=>{
	return (
			<select {...props} className='form-control'>
			<option>...</option>
			
			{	
				props.options.map(opt=>{
					return <option value={opt} selected={opt === props.value}>{opt}</option>
				
				}
			)
			
			}
			
			</select>
	)
	
}


export default Formulario