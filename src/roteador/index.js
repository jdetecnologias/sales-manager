import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import Estoque from '../view/estoque.view.js'
import Produto from '../view/produto.view.js'
import ListaProdutos from '../view/listaProdutos.view'
import CadCliente from '../view/cadastroCliente.view.js'
import LoginPage from '../view/login.view'

// import { Container } from './styles';

function Roteador () {

        return(
                <BrowserRouter>
                    <Switch>
                        <Route path="/" exact component={LoginPage}/>
                        <Route path="/produto"  component={Produto}/> 
						<Route path="/lista"  component={ListaProdutos}/> 	
						<Route path="/cadastro"  component={CadCliente}/> 	
						<Route path="/estoque"  component={Estoque}/> 		
						<Route path="/login" component={LoginPage}/>						
                    </Switch>
                </BrowserRouter>
		)		
}



export default Roteador;