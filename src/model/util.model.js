import * as U from '../utils/utils'

export function getCepModel(cep){
	const lCep = U.getLSItem('cep')
	
	if (cep.replace('-','') === lCep.cep.replace('-','') ){
		return lCep
	}else{
		return null
	}
}
	
	 