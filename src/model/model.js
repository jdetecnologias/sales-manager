import axios from 'axios'
import configs from '../configs'

const axiosInstance = axios.create({
    baseURL: configs.api_url,
    timeout: 30000
})

export function Register(ATable, AData){
    console.log(AData)
    const lUrl = '/' + ATable

    axiosInstance.post(lUrl, AData).then(results=>{

        console.log(results)
       if(!results.data.status){
           alert('Deu errado' + results.data.msn )
       }else{
           alert('Deu Certo' + results.data.msn)
       }
    }).catch(error=>{
        console.log(error)
    })
}