import {RetornarDadosTabela} from '../funcoes/funcoes'
import * as U from '../utils/utils'

export function alocarEstoqueModel(id, qtd,tipo = "alocar"){
	let lEstoque = U.getLSItem('estoque')
	
	for (let i = 0 ; i < lEstoque.length ; i++){
		for ( let j = 0 ; j < lEstoque[i].length ; j++){
			
			if(lEstoque[i][j].id == id){
				switch(tipo){
					case "alocar":
						let qtdEmEstoque = parseFloat(lEstoque[i][j].qtd)
						let saldo = qtdEmEstoque - parseFloat(qtd)
						
						if(saldo >= 0){
							lEstoque[i][j].qtd = saldo
							lEstoque[i][j].alocado = qtd
							
							U.setLSItem('estoque', lEstoque)
							return true
						}
					break;
					case "desalocar":
						let qtdAlocado = parseFloat(lEstoque[i][j].alocado)
						let qtdRestanteAlocado = qtdAlocado - parseFloat(qtd)
						
						if(qtdRestanteAlocado >= 0){
							lEstoque[i][j].qtd += qtd
							lEstoque[i][j].alocado = qtdRestanteAlocado
							
							U.setLSItem('estoque', lEstoque)
							return true
						}					
					break;
				}
			}
		}	
	}
	
	return false
}


export function criarEstoqueModel(produto){
	
	function ProdutoJaEstaNaListaEstoque(id, listaEstoque){
		return  listaEstoque.some(est=>est[0].id == id)
	}
	
	const listaProdutoVariantes = RetornarDadosTabela(produto)
	let lEstoque =  U.getLSItem('estoque')
	
	listaProdutoVariantes.forEach((lv,indice)=>{
		listaProdutoVariantes[indice].qtd = 0
		listaProdutoVariantes[indice].alocado = 0
		
	})
	
	if(lEstoque == null){
		
		lEstoque = new Array(listaProdutoVariantes)
		
	}else{

			if (!ProdutoJaEstaNaListaEstoque(produto.id, lEstoque)){
				lEstoque.push(listaProdutoVariantes)
			}
			

		

		}	
	U.setLSItem('estoque', lEstoque)
	
}

export function liberarEstoqueModel(pedido){
	const lEstoque = U.getLSItem('estoque')

	for (let i = 0 ; i < lEstoque.length ; i++){
		for ( let j = 0 ; j < lEstoque[i].length ; j++){				
			if(lEstoque[i][j].id == pedido.idProduto){
				lEstoque[i][j].alocado -= pedido.qtd
			}
		}	
	}			
		
	U.setLSItem('estoque', lEstoque)	
}