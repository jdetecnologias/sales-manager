import * as U from '../utils/utils'
import {Register} from './model'

export function gravarClienteModel(AClient){
	Register('client',AClient)
}

export function clienteExisteModel(identificador){ 
	let clientes = U.getLSItem('clientes')
	let Resultado = false
	
	if(clientes !== null){	
		clientes.forEach(cl=>{
			
			if(cl.identificador === identificador){
				Resultado =  true;
			}
		})
	}
	
	return Resultado
}

export function retornarClienteModel(identificador){
	let lClientes = U.getLSItem('clientes')
	let Resultado = null
	
	lClientes.forEach(cliente=>{
		if(cliente.identificador === identificador){
			Resultado = cliente
		}
	})
	
	return Resultado
}

export function AtualizarClienteModel(Acliente){
	let lClientes = U.getLSItem('clientes')
	let Resultado = null
	
	lClientes.forEach((cliente,index)=>{
		if(Acliente.identificador === cliente.identificador){
			lClientes[index] = cliente
		}
	})
	
	U.setLSItem('clientes',lClientes)
}



