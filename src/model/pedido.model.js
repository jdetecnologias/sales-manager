import * as U from '../utils/utils'
import {alocarEstoqueModel, liberarEstoqueModel} from './estoque.model'

function obterPedidoVazioModel(){
	return {dadosCliente:null, status:'em aberto', itens: []}
}


export function gravarPedidoModel(Apedido){
	U.setLSItem('pedido',Apedido)	
}

export function obterPedidoModel(){
	const lPedido = U.getLSItem('pedido')
	
	if(lPedido == null){
		return obterPedidoVazioModel()
	}else{
		return lPedido
	}
}


export function apagarPedidoModel(){
	const lPedido = U.getLSItem('pedido')
	
	lPedido.itens.forEach((pedido)=>{
		excluirItemModel(pedido.idProduto)
	})
	
}

export function excluirItemModel(id){
	let lPedido = U.getLSItem('pedido')
	
	lPedido.itens.forEach((ped,key)=>{
		if(ped.idProduto == id){
			
			alocarEstoqueModel(ped.idProduto, ped.qtd, "desalocar")
			
			lPedido.itens.splice(key,1)
		}
	})
	
	U.setLSItem('pedido', lPedido)
}

export function finalizarPedidoModel(APedido){
	let lPedido = APedido
	
	let lPedidos = U.getLSItem('pedidos')
	
	if(lPedido){
			
		lPedido.itens.forEach(ped=>{
			liberarEstoqueModel(ped)
		})
		
		U.setLSItem('pedido',obterPedidoVazioModel())
		
		lPedido.status = 'concluido'
		
		if(lPedidos){
			lPedidos.push(lPedido)
		}else{
			lPedidos = []
			lPedidos.push(lPedido)
		    
		}
		
		U.setLSItem('pedidos', lPedidos)
	}	
}
