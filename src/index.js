import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Produto from './view/produto.view'

import Estoque from './view/estoque.view'

import Roteador from './roteador/'


ReactDOM.render(
  <React.StrictMode>
    <Roteador/>,
  </React.StrictMode>,
  document.getElementById('root')
);
