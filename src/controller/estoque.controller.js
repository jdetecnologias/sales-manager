import {criarEstoqueModel, alocarEstoqueModel} from '../model/estoque.model'
import * as U from '../utils/utils'

export function criarEstoque(produto){
	criarEstoqueModel(produto)
	
}

export function alocarEstoque(id, qtd, tipo = "alocar"){
	
	alocarEstoqueModel(id, qtd, tipo)
}

export function setQtdItem(id, qtd){
	const estoque = U.getLSItem('estoque')
	
	estoque.forEach((variantes,key)=>{
		variantes.forEach((variante,indiceVariante)=>{
			if(variante.id == id){
				estoque[key][indiceVariante].qtd = qtd
				return
			}
		})
	})
	
	U.setLSItem('estoque', estoque)
	
}


export function getQtdItem(id, estoque, tipo = "disponivel"){
	var qtd
	
	estoque.forEach((variantes,key)=>{
		variantes.forEach((variante,indiceVariante)=>{
			if(variante.id == id){
				switch(tipo){
					case "disponivel":
						qtd =  estoque[key][indiceVariante].qtd
					break;
					case "alocado":
						qtd = estoque[key][indiceVariante].alocado
					break;
				}
								
			}
		})
	})	
	
	return qtd
}


export function verificarSeExisteEstoque(id, estoque, qtd){
	
	var resultado = false
	let lId = id+""
	const indiceNullTxt = lId.search('null')
	if(indiceNullTxt > -1){
		lId = lId.substr(0,indiceNullTxt)
	}
	
	estoque.forEach((variantes,key)=>{
		variantes.forEach((variante,indiceVariante)=>{
			const idVar = ""+variante.id
			
			if(idVar.search(lId) > -1 && parseInt(variante.qtd) >= qtd){
				resultado = true			
			}
		})
	})	
	
	return resultado
}
