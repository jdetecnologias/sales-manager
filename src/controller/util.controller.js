import {getCepModel} from '../model/util.model'

export function getCEP(cep){
	return  getCepModel(cep)
}