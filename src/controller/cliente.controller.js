import {gravarClienteModel, clienteExisteModel, AtualizarClienteModel} from '../model/cliente.model'
import * as U from '../utils/utils'

export function gravarCliente(cliente){
	gravarClienteModel(cliente)
}

export function VerificarSeClienteExiste(identificador){
	return clienteExisteModel(identificador)
}

export function RetornarClienteSessao(){
	return U.getLSItem('cliente')
}

export function atualizarCliente(cliente){
	AtualizarClienteModel(cliente)
}
