

import {gravarPedidoModel, obterPedidoModel, apagarPedidoModel, excluirItemModel, finalizarPedidoModel} from '../model/pedido.model'

export function gravarPedido(pedido){
	gravarPedidoModel(pedido)
	
}

export function obterPedido(){
	return obterPedidoModel()
}

export function apagarPedido(){
	apagarPedidoModel()
}

export function excluirItemPedido(id){
	excluirItemModel(id)
}

export function finalizarPedido(APedido){
	finalizarPedidoModel(APedido)
}